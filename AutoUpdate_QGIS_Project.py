import os
from qgis.core import QgsProject
from pyQGIS_autoupdate_bib import update_baselayers, update_taskgroups, update_sensordata

def main():
    project_path = QgsProject.instance().fileName()
    if not project_path:
        raise FileNotFoundError("No QGIS project is currently open.")
    
    # Update Background Layers:
    group_name = 'BaseLayer'
    data_folder = 'Background_Layer'
    # update_baselayers(project_path, data_folder, group_name)

    # Update Task Groups Layer:
    data_folder = 'TaskGroup_Layer'
    group_name = 'Task Groups'
    # update_taskgroups(project_path, data_folder, group_name)

    # Update Sensor Data Layer:
    group_name = 'Sensor Data'
    data_folder = 'Sensordata_Layer'
    update_sensordata(project_path, data_folder, group_name)

# Run the main function
main()