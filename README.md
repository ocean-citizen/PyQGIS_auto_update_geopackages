# PyQGIS_auto_update_geopackages

A Python function designed to automate updating the Layers in the QGIS project. All packages from the specified directory will be imported to the QGIS project, and an already existing layer will be updated. 

Patch v 0.2.0

Significant restructuring. Moven input arguments form subfunctions and merged all subfunctions in a single file.