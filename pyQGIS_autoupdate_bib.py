# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 10:02:12 2024

@author: misch
"""
import os
from qgis.core import (
    QgsProject,
    QgsVectorLayer,
    QgsMarkerSymbol,
    QgsSvgMarkerSymbolLayer
)

def update_baselayers(project_path, data_folder, group_name):
    # Get the current project directory
    
    if not project_path:
        raise FileNotFoundError("No QGIS project is currently open.")
    
    project_dir = os.path.dirname(project_path)
    
    # Define absolute paths
    geopackage_folder = os.path.join(project_dir, data_folder, 'Geopackages')
    styles_folder = os.path.join(project_dir, data_folder, 'Styles')
    svg_marker_path = os.path.join(styles_folder, 'Building-partners_wBG.svg')
    
    if not os.path.exists(geopackage_folder):
        raise FileNotFoundError(f"Geopackages folder not found: {geopackage_folder}")
    
    # Get the QGIS project instance
    project = QgsProject.instance()
    
    # Create or get the group
    root = project.layerTreeRoot()
    group = root.findGroup(group_name)
    if group is None:
        group = root.addGroup(group_name)
    
    # Get list of .gpkg files and reverse the order
    gpkg_files = [f for f in os.listdir(geopackage_folder) if f.endswith('.gpkg')]
    if not gpkg_files:
        raise FileNotFoundError(f"No .gpkg files found in: {geopackage_folder}")
    
    gpkg_files.reverse()
    
    # Iterate through all .gpkg files in reverse order
    for file_name in gpkg_files:
        gpkg_path = os.path.join(geopackage_folder, file_name)
        
        # Load the .gpkg file
        layer_name = os.path.splitext(file_name)[0]
        existing_layer = project.mapLayersByName(layer_name)
        
        if existing_layer:
            # Update the existing layer
            layer = existing_layer[0]
        else:
            # Load the new .gpkg file
            layer = QgsVectorLayer(gpkg_path, layer_name, 'ogr')
            if not layer.isValid():
                print(f"Failed to load layer: {gpkg_path}")
                continue
            
            # Add the layer to the project
            project.addMapLayer(layer, False)
            group.addLayer(layer)
        
        # Apply corresponding style if it exists
        style_path = os.path.join(styles_folder, layer_name + '.qml')
        if os.path.exists(style_path):
            layer.loadNamedStyle(style_path)
            layer.triggerRepaint()
            print(f"Applied style: {style_path}")
        else:
            print(f"No style file found for layer: {file_name}")
        
        # Apply SVG marker symbology if the layer is "OC_Partners"
        if layer.name() == "OC_Partners":
            if not os.path.exists(svg_marker_path):
                print(f"SVG file not found: {svg_marker_path}")
                continue
            
            # Create an SVG marker symbol layer
            svg_marker = QgsSvgMarkerSymbolLayer(svg_marker_path)
            svg_marker.setSize(8)  # Set marker size to 8
            
            # Create a marker symbol and add the SVG marker layer
            symbol = QgsMarkerSymbol()
            symbol.changeSymbolLayer(0, svg_marker)
            
            # Apply the symbol to the layer
            renderer = layer.renderer()
            renderer.setSymbol(symbol)
            layer.triggerRepaint()
            print(f"Applied SVG marker to layer: {layer.name()}")

    print(f"Imported and styled layers into group: {group_name}")

###

def update_taskgroups(project_path, data_folder, group_name):
    # Get the current project directory
    
    if not project_path:
        raise FileNotFoundError("No QGIS project is currently open.")
    
    project_dir = os.path.dirname(project_path)
    
    # Define absolute paths
    geopackage_folder = os.path.join(project_dir, data_folder, 'Geopackages')
    
    if not os.path.exists(geopackage_folder):
        raise FileNotFoundError(f"Geopackages folder not found: {geopackage_folder}")
    
    # Get the QGIS project instance
    project = QgsProject.instance()

    # Create or get the group
    root = project.layerTreeRoot()
    group = root.findGroup(group_name)
    if group is None:
        group = root.addGroup(group_name)
    
    # Get list of .gpkg files and reverse the order
    gpkg_files = [f for f in os.listdir(geopackage_folder) if f.endswith('.gpkg')]
    if not gpkg_files:
        raise FileNotFoundError(f"No .gpkg files found in: {geopackage_folder}")
    
    # gpkg_files.reverse()
    
    # Iterate through all .gpkg files in reverse order
    for file_name in gpkg_files:
        gpkg_path = os.path.join(geopackage_folder, file_name)
        
        # Load the .gpkg file
        layer_name = os.path.splitext(file_name)[0]
        existing_layer = project.mapLayersByName(layer_name)
        
        if existing_layer:
            # Remove the existing layer
            project.removeMapLayer(existing_layer[0])
        
        # Load the new .gpkg file
        layer = QgsVectorLayer(gpkg_path, layer_name, 'ogr')
        if not layer.isValid():
            print(f"Failed to load layer: {gpkg_path}")
            continue
        
        # Add the layer to the project without adding it to the root
        project.addMapLayer(layer, False)
        group.addLayer(layer)
        
        # Get the features from the layer to access attributes
        features = layer.getFeatures()
        
        for feature in features:
            # Get attributes for style and svg
            style_url = feature['Style_URL']
            svg_url = feature['svg_URL']
            
            style_path = os.path.join(project_dir, data_folder, style_url)
            svg_path = os.path.join(project_dir, data_folder, svg_url)
            
            # Apply corresponding style if it exists
            if os.path.exists(style_path):
                layer.loadNamedStyle(style_path)
                layer.triggerRepaint()
                print(f"Applied style: {style_path}")
            else:
                print(f"No style file found for layer: {layer_name}")
            
            # Apply SVG marker symbology if the 'svg_URL' field is not empty
            if svg_url and os.path.exists(svg_path):
                # Create an SVG marker symbol layer
                svg_marker = QgsSvgMarkerSymbolLayer(svg_path)
                svg_marker.setSize(8.5)  # Set marker size to 8.5
                
                # Create a marker symbol and add the SVG marker layer
                symbol = QgsMarkerSymbol()
                symbol.changeSymbolLayer(0, svg_marker)
                
                # Apply the symbol to the layer
                renderer = layer.renderer()
                renderer.setSymbol(symbol)
                layer.triggerRepaint()
                print(f"Applied SVG marker to layer: {layer.name()}")
            elif svg_url:
                print(f"SVG file not found: {svg_path}")
        
        # Set custom property to auto-apply styles and symbologies
        layer.setCustomProperty('apply_symbology_renderers', True)
        layer.triggerRepaint()

    print("Imported and styled layers.")


def update_sensordata(project_path, data_folder, group_name):
    # Get the current project directory
    if not project_path:
        raise FileNotFoundError("No QGIS project is currently open.")
    
    project_dir = os.path.dirname(project_path)
    
    # Define absolute paths
    geopackage_folder = os.path.join(project_dir, data_folder, 'Geopackages')
    styles_folder = os.path.join(project_dir, data_folder, 'Styles')
    
    if not os.path.exists(geopackage_folder):
        raise FileNotFoundError(f"Geopackages folder not found: {geopackage_folder}")
    
    # Get the QGIS project instance
    project = QgsProject.instance()
    
    # Create or get the "Tenerife" group
    
    root = project.layerTreeRoot()
    group = root.findGroup(group_name)
    if group is None:
        group = root.addGroup(group_name)
    
    # Get list of .gpkg files
    gpkg_files = [f for f in os.listdir(geopackage_folder) if f.endswith('.gpkg')]
    if not gpkg_files:
        raise FileNotFoundError(f"No .gpkg files found in: {geopackage_folder}")
    
    # Iterate through all .gpkg files
    for file_name in gpkg_files:
        gpkg_path = os.path.join(geopackage_folder, file_name)
        
        # Load the .gpkg file
        layer_name = os.path.splitext(file_name)[0]
        existing_layers = project.mapLayersByName(layer_name)
        
        if existing_layers:
            # Remove existing layers
            for existing_layer in existing_layers:
                project.removeMapLayer(existing_layer)
        
        # Load the new .gpkg file
        layer = QgsVectorLayer(gpkg_path, layer_name, 'ogr')
        if not layer.isValid():
            print(f"Failed to load layer: {gpkg_path}")
            continue
        
        # Add the layer to the project without adding it to the root
        project.addMapLayer(layer, False)
        group.addLayer(layer)
        
        # Get the features from the layer to access attributes
        features = layer.getFeatures()
        
        for feature in features:
            # Get the 'URL_Style' attribute value
            style_file_name = feature['URL_Style']
            if not style_file_name:
                print(f"No 'URL_Style' found for layer: {layer_name}")
                continue
            
            style_path = os.path.join(styles_folder, style_file_name)
            
            # Apply corresponding style if it exists
            if os.path.exists(style_path):
                layer.loadNamedStyle(style_path)
                layer.triggerRepaint()
                print(f"Applied style: {style_path}")
            else:
                print(f"No style file found for layer: {layer_name} with style {style_path}")

        # Set custom property to auto-apply styles and symbologies
        layer.setCustomProperty('apply_symbology_renderers', True)
        layer.triggerRepaint()

    print("Imported and styled layers.")


